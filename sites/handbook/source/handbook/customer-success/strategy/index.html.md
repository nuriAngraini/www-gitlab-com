---
layout: handbook-page-toc
title: "Customer Success Strategy and Priorities"
description: "Create a company-wide customer success approach, providing an engagement
  framework for the Customer Success organization"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Strategy and Priorities

### Q3 and Q4 of Fiscal Year 2022

[Current link for a document (GitLab internal only)](https://docs.google.com/document/d/1urqPBu_0aQpYPRExpC6DUffjkBInXP8HntH49E1JKdA/edit)This will be converted to a handbook page by 2021-08-01.

